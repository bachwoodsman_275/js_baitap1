/**
 * Bài 1
 *  Input: 
 *          +soNgayLam: number
 * Progress: 
 *          +lấy thông tin người dùng nhập từ giao diện
 *          +luongNhanVien = soNgayLam * luong1ngay
 * Ouput:
 *          +luongNhanVien
 */
document.getElementById('btnLuongNhanVien').onclick = function (){
    const LUONGMOTNGAY = 100000;
    var soNgayLam = document.getElementById('soNgayLam').value;
    var tongLuongNhanVien = 0;
    tongLuongNhanVien = soNgayLam * LUONGMOTNGAY;
    document.getElementById('result').innerText = `Lương nhân viên là: ${tongLuongNhanVien}`;
};
/**
 * Bài 2 
 * Input: 
 *      +soThuNhat : number
 *      +soThuHai : number
 *      +soThuBa : number
 *      +soThuTu : number
 *      +soThuNam : number
 * Progress:
 *      +lấy thông tin người dùng nhập từ giao diện
 *      +trungBinhCong = (soThuNhat + soThuHai + soThuBa + soThuTu + soThuNam) / 5
 * Output:
 *      +trungBinhCong
 */
document.getElementById('btnTrungBinh').onclick = function () {
    const SO = 5;
    var soThuNhat = Number(document.getElementById('soThuNhat').value);
    var soThuHai = document.getElementById('soThuHai').value *1; 
    var soThuBa = document.getElementById('soThuBa').value *1;
    var soThuTu = document.getElementById('soThuTu').value *1;
    var soThuNam = document.getElementById('soThuNam').value *1;
    var tong = 0;
    tong = soThuNhat + soThuHai + soThuBa + soThuTu + soThuNam;
    var trungBinhCong = 0;
    trungBinhCong = tong / 5;
    document.getElementById('result-bai2').innerText = `Trung bình cộng là: ${trungBinhCong}`;
};
document.getElementById('btnDoiTien').onclick = function () {
    const TYGIA = 23000;
    var tienUSD = Number(document.getElementById('tienUSD').value);
    var tienVND = 0;
    tienVND = tienUSD * TYGIA;
    document.getElementById('result-bai3').innerText = `Tiền Việt là: ${tienVND}`;
};
document.getElementById('btnTinhChuViDienTich').onclick = function () {
    var chieuDai = Number(document.getElementById('chieu-dai').value);
    var chieuRong = Number(document.getElementById('chieu-rong').value);
    var chuVi = 0;
    chuVi = (chieuDai + chieuRong) * 2;
    var dienTich = 0;
    dienTich = chieuDai * chieuRong;
    document.getElementById('result-bai4').innerHTML = `Chu vi HCN là: ${chuVi} <br /> Diện tích HCN là: ${dienTich}`;
};
document.getElementById('btnTongHaiKySo').onclick = function () {
    var number = Number(document.getElementById('number').value);
    var chuSoHangChuc;
    chuSoHangChuc = Math.floor(number / 10);
    var chuSoHangDonVi = 0;
    chuSoHangDonVi = number % 10;
    var tongHaiKySo = 0;
    tongHaiKySo = chuSoHangChuc + chuSoHangDonVi;
    document.getElementById('result-bai5').innerText = `Tổng 2 ký số là: ${tongHaiKySo}`;
};